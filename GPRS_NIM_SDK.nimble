# Package

version     = "0.1.0"
author      = "GPRS_NIM_SDK"
description = "GPRS_C_SDK headers. USE it with Nimbed"
license     = "Apache 2.0"
srcDir = "src"

# Dependencies
requires "nim >= 0.19.2", "nimterop >= 0.1.0"

import os

proc generateHeaders(fromDir = "") =
    var dirs = listDirs(fromDir)
    var files =  listFiles(fromDir)

    for file in files:
        if file.splitFile().ext == ".h":
            mkDir projectDir() & "/" & srcDir & "/" & relativePath(fromDir, projectDir() & "/headers")
            writeFile(projectDir() & "/" & srcDir & "/" & relativePath(fromDir, projectDir() & "/headers") & "/" & file.splitFile().name & ".nim", "import nimterop/cimport\nstatic:\n  cDebug()\n  cDisableCaching()\n\ncIncludeDir(\"" & relativePath(fromDir, projectDir() & "/headers") & "/\")\ncImport(\"" & relativePath(file, projectDir()) & "\", recurse = true)")

    if dirs.len > 0:
        for dir in dirs:
            generateHeaders(dir)      

task generate, "Generate src files":
    if not system.existsDir "headers": 
        exec "git clone https://github.com/petrynchyn/GPRS_C_SDK.git headers --recursive"
    withDir "headers":
        generateHeaders(system.getCurrentDir())

task clean, "Clean all":
    rmDir srcDir
    rmDir "headers"
    mkdir srcDir